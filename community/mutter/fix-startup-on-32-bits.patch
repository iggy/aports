Patch-Source: https://gitlab.gnome.org/GNOME/mutter/-/commit/260a8d92dca02994f4c77d03267a779c830caa66.patch
From 260a8d92dca02994f4c77d03267a779c830caa66 Mon Sep 17 00:00:00 2001
From: Bilal Elmoussaoui <belmouss@redhat.com>
Date: Wed, 3 Apr 2024 15:08:34 +0200
Subject: [PATCH] cogl: Use uint64_t for size params

As we are using a GParamSpecUint64 for that property
which makes it fail under 32bits platforms

Fixes: 6efd4a228 ("cogl/cleanup: Use construct-only properties for
CoglBuffer")
Closes https://gitlab.gnome.org/GNOME/mutter/-/issues/3408

Part-of: <https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3683>
---
 cogl/cogl/cogl-attribute-buffer.c |  8 ++++----
 cogl/cogl/cogl-attribute-buffer.h |  6 +++---
 cogl/cogl/cogl-index-buffer.c     |  5 +++--
 cogl/cogl/cogl-index-buffer.h     |  2 +-
 cogl/cogl/cogl-pixel-buffer.c     | 12 ++++++------
 cogl/cogl/cogl-pixel-buffer.h     |  4 ++--
 6 files changed, 19 insertions(+), 18 deletions(-)

diff --git a/cogl/cogl/cogl-attribute-buffer.c b/cogl/cogl/cogl-attribute-buffer.c
index 55e296dd3e2..ee89e500ab3 100644
--- a/cogl/cogl/cogl-attribute-buffer.c
+++ b/cogl/cogl/cogl-attribute-buffer.c
@@ -51,13 +51,13 @@ cogl_attribute_buffer_init (CoglAttributeBuffer *buffer)
 
 CoglAttributeBuffer *
 cogl_attribute_buffer_new_with_size (CoglContext *context,
-                                     size_t bytes)
+                                     size_t       bytes)
 {
   CoglAttributeBuffer *buffer;
 
   buffer = g_object_new (COGL_TYPE_ATTRIBUTE_BUFFER,
                          "context", context,
-                         "size", bytes,
+                         "size", (uint64_t) bytes,
                          "default-target", COGL_BUFFER_BIND_TARGET_ATTRIBUTE_BUFFER,
                          "update-hint", COGL_BUFFER_UPDATE_HINT_STATIC,
                          NULL);
@@ -67,8 +67,8 @@ cogl_attribute_buffer_new_with_size (CoglContext *context,
 
 CoglAttributeBuffer *
 cogl_attribute_buffer_new (CoglContext *context,
-                           size_t bytes,
-                           const void *data)
+                           size_t       bytes,
+                           const void  *data)
 {
   CoglAttributeBuffer *buffer;
 
diff --git a/cogl/cogl/cogl-attribute-buffer.h b/cogl/cogl/cogl-attribute-buffer.h
index 45040f7d9cd..59876a112de 100644
--- a/cogl/cogl/cogl-attribute-buffer.h
+++ b/cogl/cogl/cogl-attribute-buffer.h
@@ -92,7 +92,7 @@ GType               cogl_attribute_buffer_get_type       (void) G_GNUC_CONST;
  */
 COGL_EXPORT CoglAttributeBuffer *
 cogl_attribute_buffer_new_with_size (CoglContext *context,
-                                     size_t bytes);
+                                     size_t       bytes);
 
 /**
  * cogl_attribute_buffer_new:
@@ -122,7 +122,7 @@ cogl_attribute_buffer_new_with_size (CoglContext *context,
  */
 COGL_EXPORT CoglAttributeBuffer *
 cogl_attribute_buffer_new (CoglContext *context,
-                           size_t bytes,
-                           const void *data);
+                           size_t       bytes,
+                           const void  *data);
 
 G_END_DECLS
diff --git a/cogl/cogl/cogl-index-buffer.c b/cogl/cogl/cogl-index-buffer.c
index 4de5205d3bf..1e4309436e0 100644
--- a/cogl/cogl/cogl-index-buffer.c
+++ b/cogl/cogl/cogl-index-buffer.c
@@ -53,13 +53,14 @@ cogl_index_buffer_init (CoglIndexBuffer *buffer)
  * indices buffer should be able to contain multiple ranges of indices
  * which the wiki design doesn't currently consider. */
 CoglIndexBuffer *
-cogl_index_buffer_new (CoglContext *context, size_t bytes)
+cogl_index_buffer_new (CoglContext *context,
+                       size_t       bytes)
 {
   CoglIndexBuffer *indices;
 
   indices = g_object_new (COGL_TYPE_INDEX_BUFFER,
                           "context", context,
-                          "size", bytes,
+                          "size", (uint64_t) bytes,
                           "default-target", COGL_BUFFER_BIND_TARGET_INDEX_BUFFER,
                           "update-hint", COGL_BUFFER_UPDATE_HINT_STATIC,
                           NULL);
diff --git a/cogl/cogl/cogl-index-buffer.h b/cogl/cogl/cogl-index-buffer.h
index 23b75d837e1..ec741831800 100644
--- a/cogl/cogl/cogl-index-buffer.h
+++ b/cogl/cogl/cogl-index-buffer.h
@@ -78,6 +78,6 @@ GType               cogl_index_buffer_get_type       (void) G_GNUC_CONST;
  */
 COGL_EXPORT CoglIndexBuffer *
 cogl_index_buffer_new (CoglContext *context,
-                       size_t bytes);
+                       size_t       bytes);
 
 G_END_DECLS
diff --git a/cogl/cogl/cogl-pixel-buffer.c b/cogl/cogl/cogl-pixel-buffer.c
index fc238580a98..39246093340 100644
--- a/cogl/cogl/cogl-pixel-buffer.c
+++ b/cogl/cogl/cogl-pixel-buffer.c
@@ -63,15 +63,15 @@ cogl_pixel_buffer_init (CoglPixelBuffer *buffer)
 
 static CoglPixelBuffer *
 _cogl_pixel_buffer_new (CoglContext *context,
-                        size_t size,
-                        const void *data,
-                        GError **error)
+                        size_t       size,
+                        const void  *data,
+                        GError     **error)
 {
   CoglPixelBuffer *pixel_buffer;
 
   pixel_buffer = g_object_new (COGL_TYPE_PIXEL_BUFFER,
                                "context", context,
-                               "size", size,
+                               "size", (uint64_t) size,
                                "default-target", COGL_BUFFER_BIND_TARGET_PIXEL_UNPACK,
                                "update-hint", COGL_BUFFER_UPDATE_HINT_STATIC,
                                NULL);
@@ -94,8 +94,8 @@ _cogl_pixel_buffer_new (CoglContext *context,
 
 CoglPixelBuffer *
 cogl_pixel_buffer_new (CoglContext *context,
-                       size_t size,
-                       const void *data)
+                       size_t       size,
+                       const void  *data)
 {
   GError *ignore_error = NULL;
   CoglPixelBuffer *buffer =
diff --git a/cogl/cogl/cogl-pixel-buffer.h b/cogl/cogl/cogl-pixel-buffer.h
index e7abe08883a..cf26a61b292 100644
--- a/cogl/cogl/cogl-pixel-buffer.h
+++ b/cogl/cogl/cogl-pixel-buffer.h
@@ -87,7 +87,7 @@ GType               cogl_pixel_buffer_get_type       (void) G_GNUC_CONST;
  */
 COGL_EXPORT CoglPixelBuffer *
 cogl_pixel_buffer_new (CoglContext *context,
-                       size_t size,
-                       const void *data);
+                       size_t       size,
+                       const void  *data);
 
 G_END_DECLS
-- 
GitLab

